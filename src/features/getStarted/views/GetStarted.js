import React from 'react'
import {View, Text, StyleSheet, Image, TouchableOpacity, Dimensions} from 'react-native'
import { useNavigation } from '@react-navigation/native';

const {width} = Dimensions.get('window');

function GetStartedScreen(){
    const navigation = useNavigation();
    return(
        <View style={styles.viewContainer}>
            <Image source = {require('../../../assets/images/img.png')} style={styles.image}/>
            <View style={styles.boxView}>
                <Text style={styles.text1}>WAY LESS ADMIN,</Text>
                <Text style={styles.text2}>WAY MORE MONEY!</Text>
                <Text style={styles.text3}>MADE MORE AGENCIES</Text>

                <View style={styles.button}>
                    <TouchableOpacity onPress={()=> navigation.navigate('Main')}>
                        <Text style={styles.text4}>Get Started</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    viewContainer:{
        flex:1, 
        backgroundColor:'#F6F7FC'
    },
    image:{
        width: width,
        height:400,
        alignSelf:'center'
    },
    boxView:{
        alignItems:'center'
    },
    text1:{
        marginTop:80,
        fontSize:25,
        color:'#090A0C'
    },
    text2:{
        fontSize:27,
        fontWeight:'bold',
        color:'#090A0C'
    },
    text3:{
        fontSize:28,
        color:'#090A0C'
    },
    text4:{
        fontSize:17,
        letterSpacing:5,
        alignSelf:'center',
        color:'#F6F7FC'
    },
    button:{
        width: 250,
        height:50,
        backgroundColor:'#090A0C',
        marginTop:50,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:5
    
    }
})

export default GetStartedScreen;