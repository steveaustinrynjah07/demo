import React from 'react';
import {View, Text, StyleSheet, Image, Dimensions, TouchableOpacity} from 'react-native';
import { useNavigation, DrawerActions } from '@react-navigation/native';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer';


const {width} = Dimensions.get('window');

function CustomHeader(){
    const navigation = useNavigation();
    
    return(
        <View style={styles.header}>
            <View style={{flexDirection:'row',justifyContent:'space-around', alignItems:'center'}}>
                <TouchableOpacity onPress={()=> navigation.dispatch(DrawerActions.openDrawer())}>
                    <Image source={require('../../../assets/images/menu.png')} style={{width:15, height:25, marginLeft:15,tintColor: '#F6F7FC' }}/>
                </TouchableOpacity>

                <TouchableOpacity >
                    <Image source={require('../../../assets/images/search.png')} style={{width:30, height:25, marginLeft:300, tintColor: '#F6F7FC' }}/>
                </TouchableOpacity>  
            </View>
        </View>
    )
  }

  function CustomHeaderDemo(){
      return(
          <View style={styles.header}>
            <View style={{justifyContent:'center', alignItems:'center'}}>
                <TouchableOpacity >
                    <Image source={require('../../../assets/images/search.png')} style={{width:30, height:25, marginLeft:330, tintColor: '#F6F7FC' }}/>
                </TouchableOpacity> 
            </View>
          </View>
      )
  }


function HomeScreen(){
    return(
     <View style={{flex:1}}>  
        <CustomHeader/>
        <View style={styles.viewContainer}>
            <Text>Project Home Screen</Text>
        </View>
    </View>    
    )
}

function ProjectScreen(){
    return(
    <View style={{flex:1}}> 
      <CustomHeaderDemo/>
        <View style={styles.viewContainer}>
            <Text>Project Screen</Text>
        </View>
    </View>    
    )
}

function ProfileScreen(){
    return(
        <View style={{flex: 1}}>
            <CustomHeader/>
            <View style={styles.viewContainer}>
                <Text>Profile Screen</Text>
            </View>
        </View>
    )
}

function SettingScreen(){
    return(
        <View style={{flex: 1}}>
            <CustomHeader/>
            <View style={styles.viewContainer}>
                <Text>Setting Screen</Text>
            </View>
        </View>
    )
}

function LogoutScreen(){
    return(
        <View style={{flex: 1}}>
            <CustomHeader/>
            <View style={styles.viewContainer}>
                <Text>Logout Screen</Text>
            </View>
        </View>
    )
}


function CustomDrawer(props){
    return(
              <View style={{flex:1}}>
                  <View style={{height: 200, backgroundColor:'#090A0C'}}>
                    <Text style={{color: '#909090',letterSpacing:3, marginTop:10, marginLeft:5}}>WORKPLACE</Text>
                  </View>
                  <DrawerContentScrollView {...props}>
                    <DrawerItemList {...props}/>
                  </DrawerContentScrollView>
              </View>
    )
  }
  
  const Drawer = createDrawerNavigator();
  
  function NavDrawer(){
    return(
      <Drawer.Navigator initialRouteName="Home" drawerType='slide' drawerStyle={{width: width * 2/3, marginTop:'5%'}}
                        drawerContent={(props)=> <CustomDrawer {...props}/>}
                        drawerContentOptions={{activeTintColor: '#FFF',inactiveTintColor:'#FFF', style:{backgroundColor: '#090A0C'}}}>
          <Drawer.Screen name="Home" component={HomeScreen} 
              options={{
                  drawerIcon:
                  ({focused}) =>(
                <View style={{alignItems:'center', justifyContent:'space-evenly'}}>
                    <Image
                        source={require('../../../assets/images/home.png')}
                        style={{ width:24, height:24, tintColor: focused ? '#F6F7FC' : '#909090'}}
                    />
                </View>
            )
              }}
          />
          <Drawer.Screen name="Profile" component={ProfileScreen} 
              options={{
                  drawerIcon:
                  ({focused}) =>(
                <View style={{alignItems:'center', justifyContent:'space-evenly'}}>
                    <Image
                        source={require('../../../assets/images/profile.png')}
                        style={{ width:24, height:24, tintColor: focused ? '#F6F7FC' : '#909090'}}
                    />
                </View>
            )
              }}
          />

          <Drawer.Screen name="Setting" component={SettingScreen} 
                        options={{
                            drawerIcon:
                            ({focused}) =>(
                            <View style={{alignItems:'center', justifyContent:'space-evenly'}}>
                                <Image
                                    source={require('../../../assets/images/setting.png')}
                                    style={{ width:24, height:24, tintColor: focused ? '#F6F7FC' : '#909090'}}
                                />
                            </View>
                        )
                        }}
                    /> 

          <Drawer.Screen name="Logout" component={LogoutScreen} 
                        options={{
                            drawerIcon:
                            ({focused}) =>(
                            <View style={{alignItems:'center', justifyContent:'space-evenly'}}>
                                <Image
                                    source={require('../../../assets/images/logout.png')}
                                    style={{ width:24, height:24, tintColor: focused ? '#F6F7FC' : '#909090'}}
                                />
                            </View>
                        )
                        }}
                    />       
        </Drawer.Navigator>
    )
  }



const Tab = createBottomTabNavigator();

const MainTabScreen = () =>{
    return(
    <Tab.Navigator 
        initialRouteName="Home"
        tabBarOptions={{ activeTintColor: '#F6F7FC',
                         activeBackgroundColor:'#090A0C',
                         inactiveBackgroundColor: '#292831',
                         showLabel: false,
                         style:{ backgroundColor: '#292831'}
                      }}
    >
        <Tab.Screen name="Home" component={NavDrawer} 
        options={{
            tabBarIcon:({focused}) =>(
                <View style={{alignItems:'center', justifyContent:'center'}}>
                    <Image
                        source={require('../../../assets/images/home.png')}
                        style={{ width:24, height:24, tintColor: focused ? '#F6F7FC' : '#909090'}}
                    />
                    <Text style={{color: focused ? '#F6F7FC' : '#909090', fontSize:11, letterSpacing:1.5}}>
                        HOME
                    </Text>
                </View>
            )
        }}/>
        <Tab.Screen name="Project" component={ProjectScreen}  
            options={{
            tabBarIcon:({focused}) =>(
                <View style={{alignItems:'center', justifyContent:'center'}}>
                    <Image
                        source={require('../../../assets/images/project.png')}
                        style={{ width:24, height:24, tintColor: focused ? '#F6F7FC' : '#909090'}}
                    />
                    <Text style={{color: focused ? '#F6F7FC' : '#909090', fontSize:11, letterSpacing:1.5}}>
                        PROJECT
                    </Text>
                </View>
            )
        }}
        />
    </Tab.Navigator>
    )
}

const styles = StyleSheet.create({
    viewContainer:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#F6F7FC'
    },
    header:{
        flexDirection:'row', 
        height: '6%', 
        backgroundColor:'#090A0C', 
        marginTop:'5%'
    },
    customTap:{
        flexDirection:'row', 
        height: '6%',
        justifyContent:'space-evenly', 
        backgroundColor:'#090A0C', 
    },
    icon:{
        width:24,
        height:24,
    }
})

export default MainTabScreen;