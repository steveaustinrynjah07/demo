import React,{useState, useEffect} from 'react'
import {View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, ImageBackground, TextInput} from 'react-native'
import { useNavigation } from '@react-navigation/native';

const {width} = Dimensions.get('window');

function LoginScreen(){
    const navigation = useNavigation();

    const [isVisible, setIsVisible] = useState(true);

    useEffect(()=>{
        setTimeout(()=>{
            setIsVisible(false)
        },5000)
    },[])

    return(
        <View style={styles.viewContainer}>
            {isVisible ? <View style={{flex:1}}>
                <ImageBackground source={require('../../../assets/images/img.png')} blurRadius = {5} style={{width, height:'100%',}}>
                    <Image source={require('../../../assets/images/img.png')} style={{resizeMode:'contain', width:350,height:'100%', borderRadius:10, alignSelf:'center'}}/>
                </ImageBackground>
                        </View>
            :
            
                <View style={styles.loginBoxContainer}>
                    <Text style={styles.loginText}>Login</Text>

                    <Text style={styles.text}>Email :</Text>  
                        <View style={styles.boxView}>
                        <TextInput
                                placeholder="Email Address"
                                style={{marginStart:10, fontSize:17, color: '#090A0C'}}
                            /> 
                        </View>

                    <Text style={styles.text}>Password :</Text>  
                        <View style={styles.boxView}>
                        <TextInput
                                placeholder="Password"
                                secureTextEntry
                                style={{marginStart:10, fontSize:17, color: '#090A0C'}}
                            /> 
                        </View>

                    <Text style={styles.text2}>Forgot Password?</Text> 

                    <View style={styles.button}>
                        <TouchableOpacity onPress={()=> navigation.navigate('GetStarted')}>
                            <Text style={styles.text3}>LOGIN</Text>
                        </TouchableOpacity>
                    </View>

                    <Text style={{alignSelf:'center', color:'#707070', fontSize:13, fontWeight:'bold', marginTop:15}}>OR</Text>

                    <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-evenly'}}>
                        <TouchableOpacity onPress={()=> navigation.navigate('GetStarted')}>
                            <View style={styles.boxView2}>
                                <Image source = {require('../../../assets/images/facebook.png')} style={styles.icon}/>
                                <Text style={styles.text}>FACEBOOK</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={()=> navigation.navigate('GetStarted')}>
                            <View style={styles.boxView2}>
                                <Image source = {require('../../../assets/images/google.png')} style={styles.icon}/>
                                <Text style={styles.text}>GOOGLE</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    
                </View>

            
            }
        </View>
    )
}

const styles = StyleSheet.create({
    viewContainer:{
        flex: 1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#FFF',
        opacity:1
    },
    loginBoxContainer:{
        width: width/1.1,
        height: 525,
        alignSelf:'center',
        borderRadius:10,
        backgroundColor:'#F6F7FC',
        borderWidth:1,
        borderColor:'#090A0C'
        
    },
    loginText:{
        fontSize:50,
        fontWeight:'bold',
        alignSelf:'center',
        marginVertical:15,
        color:'#090A0C'
    },
    boxView:{
        flexDirection:'row',
        alignItems:'center',
        width: width/1.2,
        height:50,
        alignSelf:'center',
        backgroundColor:'#FFF',
        borderWidth:1,
        borderColor:'#090A0C',
        borderRadius:10,
        marginVertical:15,
    },
    boxView2:{
        flexDirection:'row',
        alignItems:'center',
        width: width/2.5,
        height:50,
        alignSelf:'center',
        backgroundColor:'#FFF',
        borderWidth:1,
        borderColor:'#090A0C',
        borderRadius:10,
        marginTop:10
    },
    button:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width: width/1.5,
        height:50,
        alignSelf:'center',
        borderWidth:1,
        borderColor:'#090A0C',
        backgroundColor:'#FFF',
        borderRadius:10,
        marginTop:25
    },
    icon:{
        width:30,
        height:30,
        marginLeft:10,
    },
    text:{
        fontSize:17,
        marginLeft:15,
        fontWeight:'bold',

        color:'#090A0C'
    },
    
    text2:{
        fontSize:13,
        marginLeft:230,
        marginTop:15,
        fontWeight:'bold',
        color:'#707070'
    },
    text3:{
        fontSize:17,
        marginTop:5,
        fontWeight:'bold',
        letterSpacing:5,
        color:'#707070'
    }
})

export default LoginScreen;