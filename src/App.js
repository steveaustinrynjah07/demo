import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import LoginScreen from '../src/features/auth/views/Login'
import GetStartedScreen from '../src/features/getStarted/views/GetStarted'
import MainTabScreen from '../src/features/home/views/Home'



const Stack = createStackNavigator();

function App(){
    return(
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Login">
                <Stack.Screen name="Login" component={LoginScreen} options={{headerShown: false}}/>
                <Stack.Screen name="GetStarted" component={GetStartedScreen} options={{headerShown: false}}/>
                <Stack.Screen name="Main" component={MainTabScreen} options={{headerShown: false}}/>
            </Stack.Navigator>
    </NavigationContainer>
    )
}

export default App;